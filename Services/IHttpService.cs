﻿using System;
using System.Threading.Tasks;

namespace polly_example.Services
{
    public interface IHttpService
    {
        Task<string> Ping(Uri uri);
    }
}