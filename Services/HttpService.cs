﻿using System;
using System.Net;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;

using Polly;

using polly_example.DependencyInjection;
using RestSharp;

namespace polly_example.Services
{
    public class HttpService: IHttpService
    {
        private const int maxRetryAttempts = 3;
        private readonly IRestClient _restClient;

        public HttpService(IRestClient restClient, IOptions<HttpServiceOptions> options)
        {
            _restClient = restClient;
            _restClient.BaseUrl = new Uri(options.Value.BaseUrl);
        }

        public async Task<string> Ping(Uri uri = null)
        {

            if (uri != null) { _restClient.BaseUrl = uri;}

            var serviceDownPolicy = Policy
                    .HandleResult<IRestResponse>(r => r.StatusCode == HttpStatusCode.ServiceUnavailable || r.StatusCode == HttpStatusCode.RequestTimeout)
                    .WaitAndRetryAsync(maxRetryAttempts, i => {
                        Console.WriteLine($"trying {i} times");
                        return TimeSpan.FromSeconds(1) + TimeSpan.FromMilliseconds(100);
                     });

            var result = await serviceDownPolicy
                    .ExecuteAsync(async () =>
                    {
                        RestRequest request = new RestRequest(Method.GET);
                        IRestResponse response = await _restClient.ExecuteAsync(request);
                        Console.WriteLine(response.Content);
                        return response;
                    });


            return result.Content;
        }
    }
}
