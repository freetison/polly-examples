﻿using System;
using System.Threading.Tasks;
using polly_example.Services;
using Serilog;

namespace polly_example
{
    public class App
    {
        private readonly IHttpService _httpService;


        public App(IHttpService httpService)
        {
            _httpService = httpService;
        }

        public async Task Run()
        {
            Log.Information("App started !!!");
            Console.WriteLine(await _httpService.Ping(new Uri("https://www.boredapi.com/api/activity")));

        }
    }
}
